#ifndef BUNDLE_H
#define BUNDLE_H

#include <stdbool.h>
#include "iota_types.h"
#include "transaction.h"

class BundleCTX {
public:
    unsigned char hash[NUM_HASH_BYTES]; // bundle hash, when finalized
};

namespace Bundle {


/** @brief Finalizes the bundle, if it has a valid bundle hash.
 *  A bundle is valid, if a) values sum up to 0 b) the index of each input
 *  transaction matches the provided address c) the normalized bundle hash does
 *  not contain 'M'.
 *  @param ctx the bundle context used.
 *  @param change_tx_index the index of the change transaction
 *  @param seed_bytes seed used for the addresses
 *  @param security security level used for the addresses
 *  @return true if the bundle is valid, false otherwise
 */
int validatingFinalize(BundleCTX *ctx, uint8_t change_tx_index,
                               const unsigned char *seed_bytes,
                               uint8_t security);

/** @brief Returns the (not normalized) hash of the finalized bundle.
 *  @param ctx the bundle context used
 */
const unsigned char *getHash(const BundleCTX *ctx);

/** @brief Computes the normalized hash.
 *  @param ctx the bundle context used
 *  @param hash_trytes target 81-tryte array for the normalized hash
 */
void getNormalizedHash(const BundleCTX *ctx, tryte_t *hash_trytes);

unsigned int finalize(BundleCTX *ctx, Transaction* first, bool fixMBug=true);

}

#endif // BUNDLE_H
